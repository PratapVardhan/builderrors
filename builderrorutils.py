'''
Build error utilities

builderrorutils.py yamllint <dir>               # Checks gramex.yaml at <dir>
builderrorutils.py charcount 500 1000 py,js .   # Warn if over 500 PY/JS chars, Error over 1000
builderrorutils.py charcount 500 1000 css .     # Warn if over 500 CSS chars, Error over 1000
flake8 . | builderrorutils.py flake8parse       # Split flake8 errors and warnings
eslint -f unix| builderrorutils.py eslintparse  # Split eslint errors and warnings
builderrorutils.py jscpd 100 report.json        # Formats jscpd report if over 100 duplicates
'''

from __future__ import unicode_literals
from __future__ import print_function

import csv
import gramex.cache
import gramex.config
import io
import json
import yaml
import os
import six
import sys

try:
    from pathlib2 import Path
except ImportError:
    from pathlib import Path
from fnmatch import fnmatch
from subprocess import check_output, CalledProcessError

conf = gramex.cache.open('builderrors.yaml', rel=True, Loader=yaml.FullLoader)


def jscpd(count, path):
    '''
    If number of duplicates is more than count, print duplicate lines.
    Else do not print anything.
    '''
    with io.open(path, encoding='utf-8') as handle:
        jscpd = json.load(handle)
    dups = jscpd['statistics']['duplications']
    count = int(count)
    if dups < count:
        return
    print(conf['jscpd-err'].format(dups, count))
    max_files = 10
    dups = sorted(jscpd['duplicates'], key=lambda v: v['lines'], reverse=True)
    for dup in dups[:max_files]:
        print('    - %d lines' % dup['lines'])
        print('        %(name)s:%(start)d' % dup['firstFile'])
        print('        %(name)s:%(start)d' % dup['secondFile'])
    if len(dups) > max_files:
        print('    ... %d more files' % (len(dups) - max_files))
    print('')
    sys.exit(1)


class ConfigStr(str):
    '''subclass of string to store attributes'''
    def __new__(cls, value, **kw):
        if isinstance(value, six.text_type) and six.PY2:
            value = value.encode('utf-8')
        obj = str.__new__(cls, value)
        obj.__dict__.update(kw)
        return obj


def track_substitutes(f):
    '''wrapper to return ConfigStr instance of f(x)'''
    def wrapper(*args, **kwargs):
        value = f(*args, **kwargs)
        if isinstance(value, six.string_types):
            return ConfigStr(value, __source__={'original': args[0]})
        # if the value is not a string (e.g. dict), do not provide a __source__
        return value
    return wrapper


def yamllint(path='.', gramex_init=''):
    # monkey-patch _substitute_variable with track_substitutes
    gramex.config._substitute_variable = track_substitutes(gramex.config._substitute_variable)
    gramex.config.Path = Path
    config = gramex.config.PathConfig(os.path.join(os.path.abspath(path), 'gramex.yaml')) or {}
    msg = []
    # Deploy password mandatory if auth: is present
    app_auth = config.get('app', {}).get('auth')
    url_auth = any('Auth' in x.get('handler', '')
                   for x in config.get('url', {}).values())
    if app_auth or url_auth:
        test_section = config.get('test')
        if test_section is None:
            msg.append('test: required when using auth')
        elif not test_section.get('auth'):
            msg.append('test.auth: required when using auth')

    # Ensure handlers use YAMLURL and YAMLPATH
    for name, handler in config.get('url', {}).items():
        if name == 'default':
            continue
        # Recommend $YAMLURL for url.*.pattern
        if 'pattern' in handler:
            pattern = handler.pattern
            if '/$YAMLURL/' not in pattern.__source__['original']:
                msg.append('url.{}: Use $YAMLURL in pattern:'.format(name))
        # Recommend $YAMLPATH for FileHandler path for relative paths
        if 'handler' in handler:
            if 'kwargs' in handler and 'path' in handler.kwargs:
                paths = handler.kwargs.path
                paths = (
                    handler.kwargs.path.values() if isinstance(paths, dict) else
                    paths if isinstance(paths, list) else [paths])
                for itempath in paths:
                    original = itempath.__source__['original']
                    p = os.path.normpath(itempath)
                    rel_path = os.path.abspath(p) != p
                    # If we use a relative path on a handler, use $YAMLPATH (or any variable)
                    if (handler.handler == 'FileHandler' and rel_path and '$' not in original):
                        msg.append('url.{}: Use $YAMLPATH in FileHandler path:'.format(name))
            # Deprecate DataHandler/QueryHandler if gramex init was used
            if (gramex_init and handler.handler in ['DataHandler', 'QueryHandler']):
                msg.append('url.{}: Use FormHandler not Data/QueryHandler'.format(name))

    for line in msg:
        print('    %s' % line)
    sys.exit(1 if len(msg) else 0)


node_bin = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'node_modules', '.bin')
minifiers = {
    'css': [os.path.join(node_bin, 'cleancss'), '-O2', '--inline', 'none', '{}'],
    'js': [os.path.join(node_bin, 'terser'), '{}', '--compress', '--mangle'],
    'py': ['pyminifier', '--obfuscate', '{}'],
}
skip_patterns = ['*/node_modules/*']


def skip(path):
    for pattern in skip_patterns:
        if fnmatch(path, pattern):
            return True
    return False


def charcount(warncount, errorcount, extensions, path='.'):
    extensions = set('.' + ext for ext in extensions.split(','))
    shell = 'win' in sys.platform
    counts = {}
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            target = os.path.join(dirpath, filename)
            if skip(target):
                continue
            ext = os.path.splitext(filename.lower())[-1]
            if ext in extensions:
                minifier = [part.format(target) for part in minifiers[ext[1:]]]
                try:
                    result = check_output(minifier, shell=shell)    # nosec
                except CalledProcessError:
                    print('WARNING: could not minify %s. Mail s.anand@gramener.com' % target)
                    with io.open(target, encoding='utf-8') as handle:
                        counts[target] = len(handle.read())
                else:
                    counts[target] = len(result)
    chars = sum(counts.values())
    warncount, errorcount = int(warncount), int(errorcount)
    has_error, max_files = False, 5
    for errortype, count in (('ERROR', errorcount), ('WARNING', warncount)):
        if chars > count:
            print(conf['charcount-err'].format(errortype, chars, ','.join(extensions), count))
            top_files = list(sorted(counts.items(), key=lambda v: v[1], reverse=True))
            for name, filecount in top_files[:max_files]:
                if filecount > 0:
                    print('{:10,d} {}'.format(filecount, name))
            if len(top_files) > max_files:
                print('    ... %d more files' % (len(top_files) - max_files))
            print('')
            has_error = errortype == 'ERROR'        # Set error flag only if there's an error
            break
    sys.exit(1 if has_error else 0)


def split_error_warning(warning_match, warning, error_match, error):
    '''Splits warning lines and error lines. Print them separately. Set error code if errors'''
    errors, warnings = [], []
    for line in sys.stdin:
        if warning_match in line:
            warnings.append('    ' + line)
        elif error_match in line:
            errors.append('    ' + line)
    if errors:
        print(error % len(errors))
        print(''.join(errors))
    if warnings:
        print(warning % len(warnings))
        print(''.join(warnings))
    sys.exit(1 if errors else 0)


def flake8parse():
    split_error_warning(
        warning_match=': C901 ', warning=conf['flake8-warn'],
        error_match='', error=conf['flake8-err'],
    )


def eslintparse():
    split_error_warning(
        warning_match='[Warning/', warning=conf['eslint-warn'],
        error_match='[Error/', error=conf['eslint-err'],
    )


def banditparse(validate_version):
    errors, warnings = [], []
    for row in csv.DictReader(sys.stdin):
        key = row['issue_confidence'] + '-' + row['issue_severity']
        row['desc'], err = conf['bandit-desc'][key]
        msg = ('    {filename},{line_number}: {test_id} {issue_text} {desc}\n').format(**row)
        (errors if err else warnings).append(msg)
    for txt, e in (('ERROR', errors), ('WARNING', warnings)):
        if e:
            print(conf['bandit-err'] % (txt, len(e)))
            print(''.join(e))
    sys.exit(0 if validate_version < 'v2' else 1 if errors else 0)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        globals()[sys.argv[1]](*sys.argv[2:])
    else:
        print(__doc__)
