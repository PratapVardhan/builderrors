FROM conda/miniconda3:latest

LABEL description="Gramener builderros for CI"
LABEL version="0.1"
LABEL maintainer="cto@gramener.com"

# Install nodejs and node libraries
RUN apt-get update && apt-get install -y gnupg curl wget git
RUN (curl -sL https://deb.nodesource.com/setup_10.x | bash -) && \
    apt-get install -y nodejs && npm install -g yarn
# Install psycopg2 and psutil depenedencies
RUN apt-get install -y python3-dev build-essential
# validate_files does duplicate file checking using fdupes
RUN apt-get install -qq -y libpq-dev fdupes make
# Install validate dependencies
RUN pip install flake8 flake8-gramex flake8-blind-except flake8-print flake8-debugger pep8-naming
RUN pip install bandit
RUN npm install -g eslint eslint-plugin-template eslint-plugin-html \
    stylelint stylelint-config-recommended htmllint-cli \ 
    eclint bower jscpd@0.6
# Install Gramex 1.x and dependencies
## Required for Gramex CaptureHandler tests
RUN pip install pycryptodome
## Required for Gramex CaptureHandler tests
RUN conda install -y -c conda-forge pdfminer.six
RUN pip install nose testfixtures coverage python-dateutil websocket-client sphinx_rtd_theme
## Required for gramex.ml
RUN conda install -y scikit-learn rpy2 tzlocal
RUN R -e "install.packages('backports', dependencies=TRUE, repos='https://cran.microsoft.com/')"
RUN R -e "install.packages('ggplot2', dependencies=TRUE, repos='https://cran.microsoft.com/')"
RUN R -e "install.packages('rmarkdown', dependencies=TRUE, repos='https://cran.microsoft.com/')"

# Install PhantomJS
RUN wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 && \
    tar -xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2 && \
    rm phantomjs-2.1.1-linux-x86_64.tar.bz2 && \
    apt-get install -y libfontconfig && \
    cd /usr/bin && \
    ln -s /phantomjs-2.1.1-linux-x86_64/bin/phantomjs && \
    cd /

# Install Chrome / Puppeteer dependencies
# https://github.com/GoogleChrome/puppeteer/issues/404#issuecomment-323555784
RUN apt-get install -y libpangocairo-1.0-0 libx11-xcb1 libxcomposite1 libxdamage1 libxi6 libxtst6 libnss3 libcups2 libxss1 libxrandr2 libgconf2-4 libasound2 libatk1.0-0 libgtk-3-0

# Install Selenium driver dependency: Chrome
RUN echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google-chrome.list
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN  apt-get update && apt-get -y install google-chrome-stable

# Install Selenium Chromedriver
RUN apt-get -y install unzip
RUN wget -q https://chromedriver.storage.googleapis.com/2.37/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
RUN mv chromedriver /usr/bin/chromedriver
RUN chown root:root /usr/bin/chromedriver
RUN chmod +x /usr/bin/chromedriver
RUN rm chromedriver_linux64.zip

# Install Selenium-based test automation dependences
RUN pip install selenium pytest allure-pytest pytest-html
# Install load test dependencies
RUN apt-get install -y default-jre-headless libxml2-dev libxslt-dev zlib1g-dev net-tools default-jre default-jdk zip unzip
RUN pip install bzt

# Install dev version of gramex
RUN git clone https://github.com/gramener/gramex.git
RUN pip install -e gramex && gramex setup --all && gramex license accept
# Install builderrors
RUN git clone https://PratapVardhan@bitbucket.org/PratapVardhan/builderrors.git
RUN cd builderrors && yarn install && pip install pyminifier && chmod a+x builderrors && cd /
# create validate link
RUN ln -s /builderrors/builderrors /bin/validate