docker pull conda/miniconda3:latest
docker run --name runner -i -t conda/miniconda3:latest /bin/bash

## Inside docker

# Install nodejs and node libraries
apt-get update && apt-get install -y gnupg curl wget git
(curl -sL https://deb.nodesource.com/setup_10.x | bash -)
apt-get install -y nodejs && npm install -g yarn
# Install psycopg2 and psutil depenedencies
apt-get install -y python3-dev build-essential
# validate_files does duplicate file checking using fdupes
# make is also required
apt-get install -qq -y libpq-dev fdupes make
# Install validate dependencies
pip install flake8 flake8-gramex flake8-blind-except flake8-print flake8-debugger pep8-naming
pip install bandit
npm install -g eslint eslint-plugin-template eslint-plugin-html
npm install -g stylelint stylelint-config-recommended htmllint-cli
npm install -g eclint bower jscpd@0.6
# Install Gramex 1.x and dependencies
pip install pycryptodome                            # Required for Gramex CaptureHandler tests
conda install -y -c conda-forge pdfminer.six        # Required for Gramex CaptureHandler tests
pip install nose testfixtures coverage python-dateutil websocket-client sphinx_rtd_theme
conda install -y scikit-learn                       # ModelHandler Requires scikit-learn
conda install -y rpy2 tzlocal                       # Required for gramex.ml.r()
R --vanilla <<EOF
install.packages(c('backports', 'ggplot2', 'rmarkdown'), repos='https://cran.microsoft.com/')
EOF
# pip install -U tornado==4.5.3                       # TODO: Gramex is not tested for tornado 5.x
# Install PhantomJS
wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
tar -xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2
rm phantomjs-2.1.1-linux-x86_64.tar.bz2
apt-get install -y libfontconfig
cd /usr/bin
ln -s /phantomjs-2.1.1-linux-x86_64/bin/phantomjs
cd /

# Install Chrome / Puppeteer dependencies
# https://github.com/GoogleChrome/puppeteer/issues/404#issuecomment-323555784
apt-get install -y libpangocairo-1.0-0 libx11-xcb1 libxcomposite1 libxdamage1 libxi6 libxtst6 libnss3 libcups2 libxss1 libxrandr2 libgconf2-4 libasound2 libatk1.0-0 libgtk-3-0

# Install Selenium driver dependency: Chrome
echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google-chrome.list
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
apt-get update
apt-get -y install google-chrome-stable

# Install Selenium Chromedriver
apt-get -y install unzip
wget -q https://chromedriver.storage.googleapis.com/2.37/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
mv chromedriver /usr/bin/chromedriver
chown root:root /usr/bin/chromedriver
chmod +x /usr/bin/chromedriver
rm chromedriver_linux64.zip

# Install Selenium-based test automation dependences
pip install selenium pytest allure-pytest pytest-html
# Install load test dependencies
apt-get install -y default-jre-headless libxml2-dev libxslt-dev zlib1g-dev net-tools default-jre default-jdk zip unzip
pip install bzt

# Install dev version of gramex
git clone https://github.com/gramener/gramex.git
pip install -e gramex && gramex setup --all && gramex license accept
# Install builderrors
git clone https://PratapVardhan@bitbucket.org/PratapVardhan/builderrors.git
cd builderrors && yarn install && pip install pyminifier && chmod a+x builderrors && cd /
# create validate link
ln -s /builderrors/builderrors /bin/validate

exit

## Outside docker

export VERSION=1.x
docker commit -m "Create validator image" -a "pv" runner pratapvardhan/validator:$VERSION
docker tag pratapvardhan/validator:$VERSION pratapvardhan/validator:latest
# docker rm runner
docker push pratapvardhan/validator